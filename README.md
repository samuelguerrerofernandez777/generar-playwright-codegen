## PARA GENERA TEST CON PLAYWRIGHT CODEGEN
DIRIGIRSE A LA CARPETA EN LA CUAL SE CREARA EL PROYECTO

## 01 INICIAR NODE 
```
npm init -y
```
## 02 INSTALAR PLAYWRIGHT
```
npm install playwright
```
## 03 abrir terminal  con la direccion de la carpeta creada anteriormente
```
npx playwright codegen [www.link-proyecto.com]
```
## 04 NAVEGAR Y GENERAR EL TESTING REQUERIDO UNA VEZ CULMINADO DAR  CLICK EN RECORDING DE LA VENTANA DE CODIGO QUE SE APERTURO EN EL TESTING Y COPIAR

## 05 CLICK EN EL BOTON COPIAR DE LA VENTANA DE CODIGO APERTURADA EN EL TESTING
```
pbpaste > index.js
```
este comando deberia crear un archivo ```index.js``` con el codigo copiado en el paso anterior caso contrario crear y copiar manualmente

## 06 VERIFICAR ARCHIVO TESTING - EJECUTAR  ejemplo node index.js
```
node [nombre_del_archivo].js
```
